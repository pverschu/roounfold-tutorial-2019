#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This script generates events and simulates detector smearing
#       and efficiency loss in a simplified way. The truth and reconstructed
#       events are then used to fill a response matrix. The goal of this 
#       example is to give understanding on the effects of smearing, efficiency
#       and choice of binning as a precursor for unfolding. Everything is based
#       on ROOT and RooUnfold.
#   
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

# np_theory = [down, nom, up]
np_theory = [0.45, 0.5, 0.55]


# Nuisance parameter #2 (Reco)

# Resolution of the smearing function.

# np_reco = [down, nom, up]
np_reco = [0.35, 0.4, 0.45]


sys_comb = [[1,1], [0,1], [2,1], [1,0], [1,2]]
#sys_comb = [[1,1], [0,1]]



# Smearing parameter 
bias = 0.2

# Binning
truthBins = 20
recoBins = 20

overflow = True

# Kinematic range (delta eta)
truthXMin = -4
truthXMax = 4

recoXMin = -4
recoXMax = 4

# Number of events.
nevents = 5000

# Luminosity
mcLumiFactor = 10

# Bimodal asymmetry for data.

# A quadratic efficiency.
def quad_eff(xt):

  diff = truthXMax - truthXMin

  center = truthXMin + diff/2

  xeff = 0.1 + 0.7 * (1 - ((center - xt)/diff)**2 )

  return xeff

# A linear efficiency.
def lin_eff(xt):

  xeff = 0.3 + (1.0-0.3)/(truthXMax - truthXMin)*(xt - truthXMin)

  return xeff

def smear(xt, sigma):

  from ROOT import gRandom

  xeff = lin_eff(xt)
  #xeff = quad_eff(xt)
  x= gRandom.Rndm();

  if x>xeff: return None;

  xsmear= gRandom.Gaus(bias,sigma);     #  bias and smear
  
  return xt+xsmear



def prepare_bimodal():

  import ROOT

  # Create dicts for the np variation histograms.
  resp_np_hists = {}


  sys_list = ['down','nom','up']

  # Define the locations of the peaks.
  peak_var_1 = ROOT.RooRealVar("p_1","p_1",truthXMin + 0.3*(truthXMax - truthXMin))
  peak_var_2 = ROOT.RooRealVar("p_2","p_2",truthXMin + 0.7*(truthXMax - truthXMin))

  # Define the data histogram.
  data = ROOT.TH1D ("data", "data", recoBins, recoXMin, recoXMax);

  test_truth = ROOT.TH1D("test","test",truthBins,truthXMin, truthXMax)

  # Define the width of the peaks.
  sigma_var = ROOT.RooRealVar("sigma","sigma",(truthXMax - truthXMin)*0.1)

  # Define the truth variable.
  x_truth_var = ROOT.RooRealVar("x_truth","x_truth",truthXMin,truthXMax)
  
  for i in range(len(sys_comb)):

    theory_i = sys_comb[i][0]
    reco_i = sys_comb[i][1]

    np_theory_val = np_theory[theory_i]
    np_reco_val = np_reco[reco_i]


    response = ROOT.RooUnfoldResponse (recoBins, recoXMin, recoXMax, truthBins, truthXMin, truthXMax)
    
    # Set the np value in a RooRealVar.
    fsig = ROOT.RooRealVar("fsig","signal fraction",np_theory_val,0.,1.)
    
    # Build the model.
    g1 = ROOT.RooGaussian("g1","g1",x_truth_var,peak_var_1,sigma_var)
    g2 = ROOT.RooGaussian("g2","g2",x_truth_var,peak_var_2,sigma_var)
    model = ROOT.RooAddPdf("model","model",ROOT.RooArgList(g1,g2),ROOT.RooArgList(fsig))
    
    # Generate the truth data.
    truth_data = model.generate(ROOT.RooArgSet(x_truth_var),nevents*mcLumiFactor)
    
    for truth_i in range(nevents*mcLumiFactor):        
          
      # Get the truth event.
      truth_event = truth_data.get(truth_i)
      
      # Get the truth variable.
      truth_var = truth_event.find(x_truth_var.GetName());
      
      # Get its value.
      x_truth = truth_var.getVal()
      
      # Apply the smearing function.
      x_reco = smear(x_truth, np_reco_val)
          
      # Fill the reco histogram.
      if x_reco!=None:
        response.Fill(x_reco, x_truth, 1./mcLumiFactor)
      else:
        response.Miss(x_truth, 1./mcLumiFactor)

    for i in range(0,response.Hresponse().GetNbinsX()+2):
      for j in range(0,response.Hresponse().GetNbinsY()+2):
        response.Hresponse().SetBinError(i,j,0)

    # Add the histogram to the dict.
    resp_np_hists[sys_list[theory_i]+sys_list[reco_i]] = response     

    if theory_i != 1 or reco_i != 1:
      continue

    # Generate the truth data for validation.
    truth_data = model.generate(ROOT.RooArgSet(x_truth_var),nevents*mcLumiFactor)

    for truth_i in range(nevents*mcLumiFactor):
      
      # Get the truth event.
      truth_event = truth_data.get(truth_i)
      
      # Get the truth variable.
      truth_var = truth_event.find(x_truth_var.GetName());
      
      # Get its value.
      x_truth = truth_var.getVal()

      test_truth.Fill(x_truth,1./mcLumiFactor)


    # Set the np value in a RooRealVar.
    fsig = ROOT.RooRealVar("fsig","signal fraction",0.55,0.,1.)
    
    model = ROOT.RooAddPdf("model","model",ROOT.RooArgList(g1,g2),ROOT.RooArgList(fsig))

    # Generate the truth data.
    truth_data = model.generate(ROOT.RooArgSet(x_truth_var),nevents*mcLumiFactor)    

    for truth_i in range(nevents*mcLumiFactor):        


      # Get the truth event.
      truth_event = truth_data.get(truth_i)
      
      # Get the truth variable.
      truth_var = truth_event.find(x_truth_var.GetName());
      
      # Get its value.
      x_truth = truth_var.getVal()

      # Apply the smearing function.
      x_data = smear(x_truth, np_reco_val)
    
      if x_data != None: data.Fill(x_data, 1./mcLumiFactor)

  # Create Poisson distributed observed data events.
  for i in range(recoBins + 2):
    x_data = ROOT.gRandom.Poisson(data.GetBinContent(i))
    data.SetBinContent(i, x_data)


  return resp_np_hists, data, test_truth


def save_input(resp_hists, data, test_truth):
  
  import ROOT

  outputFile = ROOT.TFile("histograms_sys.root","RECREATE");

  
  dir_nom = outputFile.mkdir("Nom")
  dir_NP1 = outputFile.mkdir("NP1")
  dir_NP2 = outputFile.mkdir("NP2")

  dir_nom.cd()
  
  resp_hists['nomnom'].Hmeasured().Write("reco")
  resp_hists['nomnom'].Htruth().Write("truth")
  resp_hists['nomnom'].Hresponse().Write("response")
  test_truth.Write("truth_test")
  data.Write("data")

  dir_NP1.cd()

  truth_dir = dir_NP1.mkdir("truth")
  resp_dir = dir_NP1.mkdir("response")
  
  truth_dir.cd()

  resp_hists['upnom'].Htruth().Write("up")
  resp_hists['downnom'].Htruth().Write("down")

  resp_dir.cd()
  
  resp_hists['upnom'].Hresponse().Write("up")
  resp_hists['downnom'].Hresponse().Write("down")


  dir_NP2.cd()

  reco_dir = dir_NP2.mkdir("reco")
  resp_dir = dir_NP2.mkdir("response")
  
  reco_dir.cd()

  resp_hists['nomup'].Hmeasured().Write("up")
  resp_hists['nomdown'].Hmeasured().Write("down")

  resp_dir.cd()
  
  resp_hists['nomup'].Hresponse().Write("up")
  resp_hists['nomdown'].Hresponse().Write("down")

  del data
  outputFile.Write()
  outputFile.Close()

def main():

  import ROOT

  # Prepare the histograms and response matrix.
  resp_hists, data, test_truth = prepare_bimodal()

  # Save the histograms to a ROOT-file.
  save_input(resp_hists, data, test_truth)
  
  print("Saved histograms in histograms_sys.root")

if __name__=="__main__":
  main()
  

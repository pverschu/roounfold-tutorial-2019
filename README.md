# RooUnfold Tutorial 2019

**=========================== Exercise 1 - Basics ===========================**

**Description:**\
The goal of this exercise is to get familiar with the basic concepts of unfolding. The truth distribution follows a bimodal
p.d.f. to which a naive detector response function is applied to create the reconstructed distribution. The entries of these 
distributions are used to build the response matrix and validation distributions. This data generation process has many parameters. 
This exercise aims to show how the following data generation parameters impact your unfolding:
- Choice of binning (tbins and rbins)
- Detector efficiency (eff)
- Detector response (Bias and sigma)
- Total number of events (nevents)
- MC and data sample size difference (mcLumiFactor)
- MC and data shape difference (frac)
- Constant background rate (nkbg)

This exercise applies the matrix inversion method which gives, like in most unfolding scenarios, bad results. However, this is strongly
coupled to the above mentioned concepts.\
\
**Assignment:**\
Adjust the parameters at the top of the script to improve the unfolding result.

\
\
**Example command:**\
    `python Ex_01_Bacics.py`

\
\
**Hints:** 
- Look in the prepare_bimodal function in helpers.py to see how the parameters influence the data.
- Try to understand the first plots before removing the return statement half-way of the script.
- Uncomment the plot_result function to visualize the unfolding result.
- Purity of reco bin i:
```math
\frac{y_{i}^{reco\&truth}}{y_{i}^{reco}}
```
- Efficiency of truth bin j:
```math
\sum_{j}\frac{y_{ij}^{reco\&truth}}{y_{j}}
```



![alt text](img/Ex_01_Basics_input.png "Input distributions")
![alt text](img/Ex_01_Basics_result.png "Unfolding result")

\
\
**========================= Exercise 2 - Regularization ======================**

**Description:**\
The goal of this exercise is to get familiar with the different regularization schemes. In real experiments one can not choose
many of the parameters mentioned in the previous exercise but has to find a regularization scheme suited for their specific case.\
\
**Assignment:**\
Find the most optimal regularization scheme by trying out different methods and strengths.


\
**Example command:**\
`python Ex_02_Regularization.py svd --regparm 7`

\
\
**Possible regularization schemes:**
- Singular Value Decomposition (svd) - Integer regularization strength between 1 and the number of truth/reco bins.
- Iterative Bayes (bayes) - Integer regularization strength of 1 or higher.
- Iterative Dynamically Stabilized (ids) - Integer regularization strenght of 1 or higher.
- TUnfold (root) - Continuous regularization strength of 0 or higher.
- Gaussian Process (gp) - Continuous regularization strength automatically found through a marginal likelihood optimization.
- *Bin-by-Bin (bbb) - Unregularized correction factors*
- *Matrix inversion (inv) - Unregularized matrix inversion*

**Hints:**
- Look at the pull of each bin to get an idea of the agreement between the truth and unfolded result.


![alt text](img/Ex_02_Regularization_bayes.png "Unfolding result")

\
\
**============================ Exercise 3 - Toy Bias =========================**

**Description:**\
The goal of this exercise is to quantify the bias of the chosen regularization scheme. There are two types of uncertainty that one
needs to take into when evaluating their unfolding result. Firstly, the uncertainty on the unfolded result itself, including 
statistical and systematic uncertainty from theory or detector effects. Secondly, one needs to take into account the bias introduced by 
the regularization scheme. These two types of uncertainty are coupled i.e. one has to find the optimal bias-variance trade-off.

\
\
**Assignment:**\
Find a trade-off between the bias and variance for your chosen regularization scheme, different bias calculations and number of toys.

\
**Example command:**\
`python Ex_03_Toybias.py svd --regparm 7 --biasmethod asi --ntoys 5`

\
\
**Possible bias methods:**
- Asimov (asi) - An Asimov distribution is created on reconstructed level, unfolded and used to throw toys. These toys are folded and unfolded of which the relative difference of the final result and the truth distribution is used to calculate the bias.
- Estimator (est) - The difference between the unfolded measured distribution and the truth distribution. No toys are thrown.
- Closure (clos) - Throw toys around the measured distribution and use the unfolded result of these toy distributions to calculate the bias.



![alt text](img/Ex_03_ToyBias_svd_asimov.png "Toy calculation")

\
\
**============================ Exercise 4 - Systematics =========================**

**Description:**\
The goal of this exercise is to show the influence of systematic uncertainties on unfolding. The supplied data now also contains
additional distributions representing up and down variations of two nuisance parameters. To generate the data run the script 
data/createHistsSys.py. It will create a root-file with all the needed distributions. 

\
\
**Assignment:**\
Create the dataset and try out different regularization schemes to investigate the effect of systematic uncertainties.

\
**Example command:**\
`python Ex_04_Sys.py root --regparm 0.01`



![alt text](img/Ex_04_Sys_root.png "Unfolding result - Systematics impact")

\
\
**============================== Exercise 5 - Higgs ============================**

**Description:**\
The goal of this exercise is to supply a realistic unfolding scenario based on a realistic dataset. The dataset is based 
on a Hγγ analysis of the ATLAS collaboration. The to-be unfolded distribution is a pt histogram where each bin count is extracted
from a corresponding mt distribution. Each bin count is the fitted number of signal events in this distribution. The fitting 
procedure can be found in the fit_histograms function in the helpers.py script. The mt distributions used for the fit are in 
data/higgs.root. This data file also contains the truth and reconstructed MC distributions used to build the response matrix.

\
All the distributions that are needed are passed back in a dictionary that can be retrieved with the same keys as in the previous exercises.

\
\
**Assignment:**\
Use the supplied dataset and the concepts of the previous exercises to get the optimal unfolding result.

**Example command:**\
`python Ex_05_Higgs.py`

![alt text](img/Ex_05_Higgs.png "Toy calculation")


\
\
**Questions:**
- Why is there a total events difference between truth and reconstructed level?
- Why is there a shape difference between truth and reconstructed level?
- Why is there a shape difference between reconstructed MC and measured data?
- Why is the data less smooth then the reconstructed MC?
- What does the response matrix tell you?
- How does the response matrix translate to the input distributions?
- What does the bin purity tell us?
- What does the reconstruction efficiency tell us?
- Why does the matrix inversion method (almost) always give results with big oscillations and large variances?
- What is a closure test and what does it test?
- What happens if you keep increasing the regularization strength of the IDS, SVD or Bayes scheme?
- What happens if you set the regularization strength of TUnfold to 0?
- Which unfolding schemes give minimal bias?
- What does an Asimov dataset represent in this context?
- What is a reasonable/tolarable amount of bias?
- What is a reasonable/tolarable amount of variance?